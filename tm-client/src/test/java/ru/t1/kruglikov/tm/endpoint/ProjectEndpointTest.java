package ru.t1.kruglikov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import ru.t1.kruglikov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.kruglikov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.kruglikov.tm.api.service.IPropertyService;
import ru.t1.kruglikov.tm.dto.model.ProjectDTO;
import ru.t1.kruglikov.tm.dto.request.project.*;
import ru.t1.kruglikov.tm.dto.request.user.UserLoginRequest;
import ru.t1.kruglikov.tm.dto.request.user.UserLogoutRequest;
import ru.t1.kruglikov.tm.dto.response.project.*;
import ru.t1.kruglikov.tm.enumerated.Status;
import ru.t1.kruglikov.tm.model.Project;
import ru.t1.kruglikov.tm.service.PropertyService;
import ru.t1.kruglikov.tm.marker.IntegrationCategory;

import java.util.List;

import static ru.t1.kruglikov.tm.constant.ProjectTestData.*;
import static ru.t1.kruglikov.tm.constant.UserTestData.*;
import static ru.t1.kruglikov.tm.constant.UserTestData.USER_PASSWORD;

@Category(IntegrationCategory.class)
public final class ProjectEndpointTest {

    @Nullable
    private String adminToken;

    @Nullable
    private String userToken;

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(propertyService);

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private String getToken(@NotNull final String login, @NotNull final String password) {
        @NotNull final UserLoginRequest request = new UserLoginRequest(null);
        request.setLogin(login);
        request.setPassword(password);

        @Nullable final String token = authEndpoint.login(request).getToken();
        return token;
    }

    @Before
    public void login() {
        adminToken = getToken(ADMIN_LOGIN, ADMIN_PASSWORD);
        userToken = getToken(USER_LOGIN, USER_PASSWORD);
        projectEndpoint.clear(new ProjectClearRequest(adminToken));
        projectEndpoint.clear(new ProjectClearRequest(userToken));
    }

    @After
    public void logout() {
        authEndpoint.logout(new UserLogoutRequest(adminToken));
        authEndpoint.logout(new UserLogoutRequest(userToken));
        userToken = null;
        adminToken = null;
    }

    @Test
    public void testCreateProject() {
        @NotNull final ProjectCreateRequest requestCreate = new ProjectCreateRequest(userToken);
        requestCreate.setName(PROJECT1_NAME);
        requestCreate.setDescription(PROJECT1_DESC);

        @NotNull final ProjectCreateResponse responseCreate = projectEndpoint.create(requestCreate);
        @Nullable ProjectDTO projectCreate = responseCreate.getProject();
        Assert.assertNotNull(projectCreate);
        Assert.assertEquals(PROJECT1_NAME, projectCreate.getName());
        Assert.assertEquals(PROJECT1_DESC, projectCreate.getDescription());

        @NotNull final ProjectListRequest requestListUser = new ProjectListRequest(userToken);
        @NotNull final ProjectListResponse responseListUser = projectEndpoint.list(requestListUser);
        @Nullable final List<ProjectDTO> projectListUser = responseListUser.getProjects();
        Assert.assertNotNull(projectListUser);
        Assert.assertEquals(1, projectListUser.size());
        Assert.assertEquals(PROJECT1_NAME, projectListUser.get(0).getName());
        Assert.assertEquals(PROJECT1_DESC, projectListUser.get(0).getDescription());

        @NotNull final ProjectListRequest requestListAdmin = new ProjectListRequest(adminToken);
        @NotNull final ProjectListResponse responseListAdmin = projectEndpoint.list(requestListAdmin);
        @Nullable final List<ProjectDTO> projectListAdmin = responseListAdmin.getProjects();
        Assert.assertNull(projectListAdmin);

        @NotNull final ProjectCreateRequest requestException = new ProjectCreateRequest(userToken);
        thrown.expect(Exception.class);
        projectEndpoint.create(requestException);
    }

    @Test
    public void testListProject() {
        @NotNull final ProjectCreateRequest requestCreate = new ProjectCreateRequest(userToken);
        requestCreate.setName(PROJECT1_NAME);
        requestCreate.setDescription(PROJECT1_DESC);
        projectEndpoint.create(requestCreate);

        requestCreate.setName(PROJECT2_NAME);
        requestCreate.setDescription(PROJECT2_DESC);
        projectEndpoint.create(requestCreate);

        requestCreate.setName(PROJECT3_NAME);
        requestCreate.setDescription(PROJECT3_DESC);
        projectEndpoint.create(requestCreate);

        @NotNull final ProjectListRequest requestList = new ProjectListRequest(userToken);
        @NotNull ProjectListResponse responseList = projectEndpoint.list(requestList);
        @Nullable List<ProjectDTO> projectList = responseList.getProjects();
        Assert.assertNotNull(projectList);
        Assert.assertEquals(3, projectList.size());
        Assert.assertEquals(PROJECT1_NAME, projectList.get(0).getName());
        Assert.assertEquals(PROJECT2_NAME, projectList.get(1).getName());
        Assert.assertEquals(PROJECT3_NAME, projectList.get(2).getName());
        Assert.assertEquals(PROJECT1_DESC, projectList.get(0).getDescription());
        Assert.assertEquals(PROJECT2_DESC, projectList.get(1).getDescription());
        Assert.assertEquals(PROJECT3_DESC, projectList.get(2).getDescription());
    }

    @Test
    public void testShowByIdProject() {
        @NotNull final ProjectCreateRequest requestCreate = new ProjectCreateRequest(userToken);
        requestCreate.setName(PROJECT1_NAME);
        requestCreate.setDescription(PROJECT1_DESC);
        projectEndpoint.create(requestCreate);

        requestCreate.setName(PROJECT2_NAME);
        requestCreate.setDescription(PROJECT2_DESC);
        @NotNull final String projectId = projectEndpoint.create(requestCreate).getProject().getId();

        requestCreate.setName(PROJECT3_NAME);
        requestCreate.setDescription(PROJECT3_DESC);
        projectEndpoint.create(requestCreate);

        @NotNull final ProjectShowByIdRequest requestShow = new ProjectShowByIdRequest(userToken);
        requestShow.setProjectId(projectId);
        @NotNull final ProjectShowByIdResponse responseShow = projectEndpoint.showById(requestShow);
        @Nullable final ProjectDTO project = responseShow.getProject();
        Assert.assertEquals(PROJECT2_NAME, project.getName());
        Assert.assertEquals(PROJECT2_DESC, project.getDescription());

        @NotNull final ProjectShowByIdRequest requestException = new ProjectShowByIdRequest(userToken);
        thrown.expect(Exception.class);
        projectEndpoint.showById(requestException);
    }

    @Test
    public void testShowByIndexProject() {
        @NotNull final ProjectCreateRequest requestCreate = new ProjectCreateRequest(userToken);
        requestCreate.setName(PROJECT1_NAME);
        requestCreate.setDescription(PROJECT1_DESC);
        projectEndpoint.create(requestCreate);

        requestCreate.setName(PROJECT2_NAME);
        requestCreate.setDescription(PROJECT2_DESC);
        projectEndpoint.create(requestCreate);

        requestCreate.setName(PROJECT3_NAME);
        requestCreate.setDescription(PROJECT3_DESC);
        projectEndpoint.create(requestCreate);

        @NotNull final ProjectShowByIndexRequest requestShow = new ProjectShowByIndexRequest(userToken);
        requestShow.setIndex(1);
        @NotNull final ProjectShowByIndexResponse responseShow = projectEndpoint.showByIndex(requestShow);
        @Nullable final ProjectDTO project = responseShow.getProject();
        Assert.assertEquals(PROJECT2_NAME, project.getName());
        Assert.assertEquals(PROJECT2_DESC, project.getDescription());

        @NotNull final ProjectShowByIndexRequest requestException = new ProjectShowByIndexRequest(userToken);
        thrown.expect(Exception.class);
        requestException.setIndex(-1);
        projectEndpoint.showByIndex(requestException);
    }

    @Test
    public void testClearProject() {
        @NotNull final ProjectCreateRequest requestCreate = new ProjectCreateRequest(userToken);
        requestCreate.setName(PROJECT1_NAME);
        requestCreate.setDescription(PROJECT1_DESC);
        projectEndpoint.create(requestCreate);

        requestCreate.setName(PROJECT2_NAME);
        requestCreate.setDescription(PROJECT2_DESC);
        projectEndpoint.create(requestCreate);

        requestCreate.setName(PROJECT3_NAME);
        requestCreate.setDescription(PROJECT3_DESC);
        projectEndpoint.create(requestCreate);

        @NotNull final ProjectListRequest requestList = new ProjectListRequest(userToken);
        @NotNull ProjectListResponse responseList = projectEndpoint.list(requestList);
        @Nullable List<ProjectDTO> projectList = responseList.getProjects();
        Assert.assertNotNull(projectList);
        Assert.assertEquals(3, projectList.size());

        @NotNull final ProjectClearRequest requestClear = new ProjectClearRequest(userToken);
        projectEndpoint.clear(requestClear);

        responseList = projectEndpoint.list(requestList);
        projectList = responseList.getProjects();
        Assert.assertNull(projectList);
    }

    @Test
    public void testRemoveByIdProject() {
        @NotNull final ProjectCreateRequest requestCreate = new ProjectCreateRequest(userToken);
        requestCreate.setName(PROJECT1_NAME);
        requestCreate.setDescription(PROJECT1_DESC);
        projectEndpoint.create(requestCreate);

        requestCreate.setName(PROJECT2_NAME);
        requestCreate.setDescription(PROJECT2_DESC);
        projectEndpoint.create(requestCreate);

        requestCreate.setName(PROJECT3_NAME);
        requestCreate.setDescription(PROJECT3_DESC);
        projectEndpoint.create(requestCreate);

        @NotNull final ProjectListRequest requestList = new ProjectListRequest(userToken);
        @NotNull ProjectListResponse responseList = projectEndpoint.list(requestList);
        @Nullable List<ProjectDTO> projectList = responseList.getProjects();
        Assert.assertNotNull(projectList);
        Assert.assertEquals(3, projectList.size());
        Assert.assertEquals(PROJECT2_NAME, projectList.get(1).getName());
        @NotNull final String projectId = projectList.get(1).getId();

        @NotNull final ProjectRemoveByIdRequest requestRemove = new ProjectRemoveByIdRequest(userToken);
        requestRemove.setProjectId(projectId);
        projectEndpoint.removeById(requestRemove);

        responseList = projectEndpoint.list(requestList);
        projectList = responseList.getProjects();
        Assert.assertEquals(2, projectList.size());
        Assert.assertNotEquals(PROJECT2_NAME, projectList.get(1).getName());

        @NotNull final ProjectRemoveByIdRequest requestException = new ProjectRemoveByIdRequest(userToken);
        thrown.expect(Exception.class);
        projectEndpoint.removeById(requestException);
    }

    @Test
    public void testRemoveByIndexProject() {
        @NotNull final ProjectCreateRequest requestCreate = new ProjectCreateRequest(userToken);
        requestCreate.setName(PROJECT1_NAME);
        requestCreate.setDescription(PROJECT1_DESC);
        projectEndpoint.create(requestCreate);

        requestCreate.setName(PROJECT2_NAME);
        requestCreate.setDescription(PROJECT2_DESC);
        projectEndpoint.create(requestCreate);

        requestCreate.setName(PROJECT3_NAME);
        requestCreate.setDescription(PROJECT3_DESC);
        projectEndpoint.create(requestCreate);

        @NotNull final ProjectListRequest requestList = new ProjectListRequest(userToken);
        @NotNull ProjectListResponse responseList = projectEndpoint.list(requestList);
        @Nullable List<ProjectDTO> projectList = responseList.getProjects();
        Assert.assertNotNull(projectList);
        Assert.assertEquals(3, projectList.size());
        Assert.assertEquals(PROJECT2_NAME, projectList.get(1).getName());
        @NotNull final String projectId = projectList.get(1).getId();

        @NotNull final ProjectRemoveByIndexRequest requestRemove = new ProjectRemoveByIndexRequest(userToken);
        requestRemove.setIndex(1);
        projectEndpoint.removeByIndex(requestRemove);

        responseList = projectEndpoint.list(requestList);
        projectList = responseList.getProjects();
        Assert.assertEquals(2, projectList.size());
        Assert.assertNotEquals(PROJECT2_NAME, projectList.get(1).getName());

        @NotNull final ProjectRemoveByIndexRequest requestException = new ProjectRemoveByIndexRequest(userToken);
        thrown.expect(Exception.class);
        requestException.setIndex(-1);
        projectEndpoint.removeByIndex(requestException);
    }

    @Test
    public void testUpdateByIdProject() {
        @NotNull final ProjectCreateRequest requestCreate = new ProjectCreateRequest(userToken);
        requestCreate.setName(PROJECT1_NAME);
        requestCreate.setDescription(PROJECT1_DESC);
        projectEndpoint.create(requestCreate);

        requestCreate.setName(PROJECT2_NAME);
        requestCreate.setDescription(PROJECT2_DESC);
        @NotNull final String projectId = projectEndpoint.create(requestCreate).getProject().getId();

        @NotNull final ProjectListRequest requestList = new ProjectListRequest(userToken);
        @NotNull ProjectListResponse responseList = projectEndpoint.list(requestList);
        @Nullable List<ProjectDTO> projectList = responseList.getProjects();
        Assert.assertNotNull(projectList);
        Assert.assertEquals(2, projectList.size());
        Assert.assertEquals(PROJECT2_NAME, projectList.get(1).getName());
        Assert.assertEquals(PROJECT2_DESC, projectList.get(1).getDescription());

        @NotNull final ProjectUpdateByIdRequest requestUpdate = new ProjectUpdateByIdRequest(userToken);
        requestUpdate.setProjectId(projectId);
        requestUpdate.setName(PROJECT3_NAME);
        requestUpdate.setDescription(PROJECT3_DESC);
        @Nullable ProjectDTO project = projectEndpoint.updateById(requestUpdate).getProject();
        Assert.assertNotNull(project);
        Assert.assertEquals(PROJECT3_NAME, project.getName());
        Assert.assertEquals(PROJECT3_DESC, project.getDescription());

        responseList = projectEndpoint.list(requestList);
        projectList = responseList.getProjects();
        Assert.assertNotNull(projectList);
        Assert.assertEquals(2, projectList.size());
        Assert.assertEquals(PROJECT3_NAME, projectList.get(1).getName());
        Assert.assertEquals(PROJECT3_DESC, projectList.get(1).getDescription());

        @NotNull final ProjectUpdateByIdRequest requestException = new ProjectUpdateByIdRequest(userToken);
        thrown.expect(Exception.class);
        projectEndpoint.updateById(requestException);
    }

    @Test
    public void testUpdateByIndexProject() {
        @NotNull final ProjectCreateRequest requestCreate = new ProjectCreateRequest(userToken);
        requestCreate.setName(PROJECT1_NAME);
        requestCreate.setDescription(PROJECT1_DESC);
        projectEndpoint.create(requestCreate);

        requestCreate.setName(PROJECT2_NAME);
        requestCreate.setDescription(PROJECT2_DESC);
        projectEndpoint.create(requestCreate).getProject().getId();

        @NotNull final ProjectListRequest requestList = new ProjectListRequest(userToken);
        @NotNull ProjectListResponse responseList = projectEndpoint.list(requestList);
        @Nullable List<ProjectDTO> projectList = responseList.getProjects();
        Assert.assertNotNull(projectList);
        Assert.assertEquals(2, projectList.size());
        Assert.assertEquals(PROJECT2_NAME, projectList.get(1).getName());
        Assert.assertEquals(PROJECT2_DESC, projectList.get(1).getDescription());

        @NotNull final ProjectUpdateByIndexRequest requestUpdate = new ProjectUpdateByIndexRequest(userToken);
        requestUpdate.setIndex(1);
        requestUpdate.setName(PROJECT3_NAME);
        requestUpdate.setDescription(PROJECT3_DESC);
        @Nullable ProjectDTO project = projectEndpoint.updateByIndex(requestUpdate).getProject();
        Assert.assertNotNull(project);
        Assert.assertEquals(PROJECT3_NAME, project.getName());
        Assert.assertEquals(PROJECT3_DESC, project.getDescription());

        responseList = projectEndpoint.list(requestList);
        projectList = responseList.getProjects();
        Assert.assertNotNull(projectList);
        Assert.assertEquals(2, projectList.size());
        Assert.assertEquals(PROJECT3_NAME, projectList.get(1).getName());
        Assert.assertEquals(PROJECT3_DESC, projectList.get(1).getDescription());

        @NotNull final ProjectUpdateByIndexRequest requestException = new ProjectUpdateByIndexRequest(userToken);
        thrown.expect(Exception.class);
        requestException.setIndex(-1);
        projectEndpoint.updateByIndex(requestException);
    }

    @Test
    public void testChangeStatusByIdProject() {
        @NotNull final ProjectCreateRequest requestCreate = new ProjectCreateRequest(userToken);
        requestCreate.setName(PROJECT1_NAME);
        requestCreate.setDescription(PROJECT1_DESC);
        projectEndpoint.create(requestCreate);

        requestCreate.setName(PROJECT2_NAME);
        requestCreate.setDescription(PROJECT2_DESC);
        @NotNull final String projectId = projectEndpoint.create(requestCreate).getProject().getId();

        @NotNull final ProjectListRequest requestList = new ProjectListRequest(userToken);
        @NotNull ProjectListResponse responseList = projectEndpoint.list(requestList);
        @Nullable List<ProjectDTO> projectList = responseList.getProjects();
        Assert.assertNotNull(projectList);
        Assert.assertEquals(2, projectList.size());
        Assert.assertEquals(PROJECT2_NAME, projectList.get(1).getName());
        Assert.assertEquals(Status.NOT_STARTED, projectList.get(1).getStatus());

        @NotNull final ProjectChangeStatusByIdRequest requestChangeStatus = new ProjectChangeStatusByIdRequest(userToken);
        requestChangeStatus.setProjectId(projectId);
        requestChangeStatus.setStatus(Status.IN_PROGRESS);
        @Nullable ProjectDTO project = projectEndpoint.changeStatusById(requestChangeStatus).getProject();
        Assert.assertNotNull(project);
        Assert.assertEquals(PROJECT2_NAME, project.getName());
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());

        responseList = projectEndpoint.list(requestList);
        projectList = responseList.getProjects();
        Assert.assertNotNull(projectList);
        Assert.assertEquals(2, projectList.size());
        Assert.assertEquals(PROJECT2_NAME, projectList.get(1).getName());
        Assert.assertEquals(Status.IN_PROGRESS, projectList.get(1).getStatus());

        @NotNull final ProjectChangeStatusByIdRequest requestException = new ProjectChangeStatusByIdRequest(userToken);
        thrown.expect(Exception.class);
        projectEndpoint.changeStatusById(requestException);
    }

    @Test
    public void testChangeStatusByIndexProject() {
        @NotNull final ProjectCreateRequest requestCreate = new ProjectCreateRequest(userToken);
        requestCreate.setName(PROJECT1_NAME);
        requestCreate.setDescription(PROJECT1_DESC);
        projectEndpoint.create(requestCreate);

        requestCreate.setName(PROJECT2_NAME);
        requestCreate.setDescription(PROJECT2_DESC);
        projectEndpoint.create(requestCreate).getProject().getId();

        @NotNull final ProjectListRequest requestList = new ProjectListRequest(userToken);
        @NotNull ProjectListResponse responseList = projectEndpoint.list(requestList);
        @Nullable List<ProjectDTO> projectList = responseList.getProjects();
        Assert.assertNotNull(projectList);
        Assert.assertEquals(2, projectList.size());
        Assert.assertEquals(PROJECT2_NAME, projectList.get(1).getName());
        Assert.assertEquals(Status.NOT_STARTED, projectList.get(1).getStatus());

        @NotNull final ProjectChangeStatusByIndexRequest requestChangeStatus = new ProjectChangeStatusByIndexRequest(userToken);
        requestChangeStatus.setIndex(1);
        requestChangeStatus.setStatus(Status.IN_PROGRESS);
        @Nullable ProjectDTO project = projectEndpoint.changeStatusByIndex(requestChangeStatus).getProject();
        Assert.assertNotNull(project);
        Assert.assertEquals(PROJECT2_NAME, project.getName());
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());

        responseList = projectEndpoint.list(requestList);
        projectList = responseList.getProjects();
        Assert.assertNotNull(projectList);
        Assert.assertEquals(2, projectList.size());
        Assert.assertEquals(PROJECT2_NAME, projectList.get(1).getName());
        Assert.assertEquals(Status.IN_PROGRESS, projectList.get(1).getStatus());

        @NotNull final ProjectChangeStatusByIndexRequest requestException = new ProjectChangeStatusByIndexRequest(userToken);
        thrown.expect(Exception.class);
        requestException.setIndex(-1);
        projectEndpoint.changeStatusByIndex(requestException);
    }

    @Test
    public void testCompleteByIdProject() {
        @NotNull final ProjectCreateRequest requestCreate = new ProjectCreateRequest(userToken);
        requestCreate.setName(PROJECT1_NAME);
        requestCreate.setDescription(PROJECT1_DESC);
        projectEndpoint.create(requestCreate);

        requestCreate.setName(PROJECT2_NAME);
        requestCreate.setDescription(PROJECT2_DESC);
        @NotNull final String projectId = projectEndpoint.create(requestCreate).getProject().getId();

        @NotNull final ProjectListRequest requestList = new ProjectListRequest(userToken);
        @NotNull ProjectListResponse responseList = projectEndpoint.list(requestList);
        @Nullable List<ProjectDTO> projectList = responseList.getProjects();
        Assert.assertNotNull(projectList);
        Assert.assertEquals(2, projectList.size());
        Assert.assertEquals(PROJECT2_NAME, projectList.get(1).getName());
        Assert.assertEquals(Status.NOT_STARTED, projectList.get(1).getStatus());

        @NotNull final ProjectCompleteByIdRequest requestChangeStatus = new ProjectCompleteByIdRequest(userToken);
        requestChangeStatus.setProjectId(projectId);
        @Nullable ProjectDTO project = projectEndpoint.completeById(requestChangeStatus).getProject();
        Assert.assertNotNull(project);
        Assert.assertEquals(PROJECT2_NAME, project.getName());
        Assert.assertEquals(Status.COMPLETED, project.getStatus());

        responseList = projectEndpoint.list(requestList);
        projectList = responseList.getProjects();
        Assert.assertNotNull(projectList);
        Assert.assertEquals(2, projectList.size());
        Assert.assertEquals(PROJECT2_NAME, projectList.get(1).getName());
        Assert.assertEquals(Status.COMPLETED, projectList.get(1).getStatus());

        @NotNull final ProjectCompleteByIdRequest requestException = new ProjectCompleteByIdRequest(userToken);
        thrown.expect(Exception.class);
        projectEndpoint.completeById(requestException);
    }

    @Test
    public void testCompleteByIndexProject() {
        @NotNull final ProjectCreateRequest requestCreate = new ProjectCreateRequest(userToken);
        requestCreate.setName(PROJECT1_NAME);
        requestCreate.setDescription(PROJECT1_DESC);
        projectEndpoint.create(requestCreate);

        requestCreate.setName(PROJECT2_NAME);
        requestCreate.setDescription(PROJECT2_DESC);
        projectEndpoint.create(requestCreate).getProject().getId();

        @NotNull final ProjectListRequest requestList = new ProjectListRequest(userToken);
        @NotNull ProjectListResponse responseList = projectEndpoint.list(requestList);
        @Nullable List<ProjectDTO> projectList = responseList.getProjects();
        Assert.assertNotNull(projectList);
        Assert.assertEquals(2, projectList.size());
        Assert.assertEquals(PROJECT2_NAME, projectList.get(1).getName());
        Assert.assertEquals(Status.NOT_STARTED, projectList.get(1).getStatus());

        @NotNull final ProjectCompleteByIndexRequest requestChangeStatus = new ProjectCompleteByIndexRequest(userToken);
        requestChangeStatus.setIndex(1);
        @Nullable ProjectDTO project = projectEndpoint.completeByIndex(requestChangeStatus).getProject();
        Assert.assertNotNull(project);
        Assert.assertEquals(PROJECT2_NAME, project.getName());
        Assert.assertEquals(Status.COMPLETED, project.getStatus());

        responseList = projectEndpoint.list(requestList);
        projectList = responseList.getProjects();
        Assert.assertNotNull(projectList);
        Assert.assertEquals(2, projectList.size());
        Assert.assertEquals(PROJECT2_NAME, projectList.get(1).getName());
        Assert.assertEquals(Status.COMPLETED, projectList.get(1).getStatus());

        @NotNull final ProjectCompleteByIndexRequest requestException = new ProjectCompleteByIndexRequest(userToken);
        thrown.expect(Exception.class);
        requestException.setIndex(-1);
        projectEndpoint.completeByIndex(requestException);
    }

    @Test
    public void testStartByIdProject() {
        @NotNull final ProjectCreateRequest requestCreate = new ProjectCreateRequest(userToken);
        requestCreate.setName(PROJECT1_NAME);
        requestCreate.setDescription(PROJECT1_DESC);
        projectEndpoint.create(requestCreate);

        requestCreate.setName(PROJECT2_NAME);
        requestCreate.setDescription(PROJECT2_DESC);
        @NotNull final String projectId = projectEndpoint.create(requestCreate).getProject().getId();

        @NotNull final ProjectListRequest requestList = new ProjectListRequest(userToken);
        @NotNull ProjectListResponse responseList = projectEndpoint.list(requestList);
        @Nullable List<ProjectDTO> projectList = responseList.getProjects();
        Assert.assertNotNull(projectList);
        Assert.assertEquals(2, projectList.size());
        Assert.assertEquals(PROJECT2_NAME, projectList.get(1).getName());
        Assert.assertEquals(Status.NOT_STARTED, projectList.get(1).getStatus());

        @NotNull final ProjectStartByIdRequest requestChangeStatus = new ProjectStartByIdRequest(userToken);
        requestChangeStatus.setProjectId(projectId);
        @Nullable ProjectDTO project = projectEndpoint.startById(requestChangeStatus).getProject();
        Assert.assertNotNull(project);
        Assert.assertEquals(PROJECT2_NAME, project.getName());
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());

        responseList = projectEndpoint.list(requestList);
        projectList = responseList.getProjects();
        Assert.assertNotNull(projectList);
        Assert.assertEquals(2, projectList.size());
        Assert.assertEquals(PROJECT2_NAME, projectList.get(1).getName());
        Assert.assertEquals(Status.IN_PROGRESS, projectList.get(1).getStatus());

        @NotNull final ProjectStartByIdRequest requestException = new ProjectStartByIdRequest(userToken);
        thrown.expect(Exception.class);
        projectEndpoint.startById(requestException);
    }

    @Test
    public void testStartByIndexProject() {
        @NotNull final ProjectCreateRequest requestCreate = new ProjectCreateRequest(userToken);
        requestCreate.setName(PROJECT1_NAME);
        requestCreate.setDescription(PROJECT1_DESC);
        projectEndpoint.create(requestCreate);

        requestCreate.setName(PROJECT2_NAME);
        requestCreate.setDescription(PROJECT2_DESC);
        projectEndpoint.create(requestCreate).getProject().getId();

        @NotNull final ProjectListRequest requestList = new ProjectListRequest(userToken);
        @NotNull ProjectListResponse responseList = projectEndpoint.list(requestList);
        @Nullable List<ProjectDTO> projectList = responseList.getProjects();
        Assert.assertNotNull(projectList);
        Assert.assertEquals(2, projectList.size());
        Assert.assertEquals(PROJECT2_NAME, projectList.get(1).getName());
        Assert.assertEquals(Status.NOT_STARTED, projectList.get(1).getStatus());

        @NotNull final ProjectStartByIndexRequest requestChangeStatus = new ProjectStartByIndexRequest(userToken);
        requestChangeStatus.setIndex(1);
        @Nullable ProjectDTO project = projectEndpoint.startByIndex(requestChangeStatus).getProject();
        Assert.assertNotNull(project);
        Assert.assertEquals(PROJECT2_NAME, project.getName());
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());

        responseList = projectEndpoint.list(requestList);
        projectList = responseList.getProjects();
        Assert.assertNotNull(projectList);
        Assert.assertEquals(2, projectList.size());
        Assert.assertEquals(PROJECT2_NAME, projectList.get(1).getName());
        Assert.assertEquals(Status.IN_PROGRESS, projectList.get(1).getStatus());

        @NotNull final ProjectStartByIndexRequest requestException = new ProjectStartByIndexRequest(userToken);
        thrown.expect(Exception.class);
        requestException.setIndex(-1);
        projectEndpoint.startByIndex(requestException);
    }

}
